import React, { Component } from 'react';
import { Route, Switch, Link } from "react-router-dom";


import HomePage from './pages/HomePage';
import AboutPage from './pages/AboutPage';
import UserPage from './pages/UserPage';

import Header from './Header';
import UsersList from './UsersList';

class App extends Component {
    render() {
        return (
            <div className="ui container">
                <Header/>
                <Switch>
                    <Route path="/" exact component={HomePage} />
                    <Route path="/about" exact component={AboutPage} />
                    <Route path="/home" exact component={HomePage} />
                    <Route path="/user/:id/:name" component={UserPage} />
                </Switch>
            </div>
        );
    }
}

export default App;