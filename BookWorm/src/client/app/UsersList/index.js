import React from 'react';
import { Link } from 'react-router-dom';
import UsersData from '../data/users';

const UsersList = () => {
    
    return <div>
        <ul>
            {
                UsersData.map(function(user){
                    return <li key={user.id}><Link to={"/user/"+user.id+"/"+user.name}>{user.name}</Link></li>
                })
            }            
        </ul>
    </div>
}

export default UsersList;