import React from 'react';

const UserPage = (props) => {
    console.log(props.match.params);
    return <div>
        <div>Id <b>{props.match.params.id}</b></div>
        <div>User <b>{props.match.params.name}</b></div>
    </div>
}

export default UserPage;